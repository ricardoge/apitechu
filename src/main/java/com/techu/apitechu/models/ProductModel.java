package com.techu.apitechu.models;

public class ProductModel {
    private Strint id;
    private String desc;
    private float price;

    public ProductModel(Strint id, String desc, float price) {
        this.id = id;
        this.desc = desc;
        this.price = price;
    }
    

    public void setId(Strint id) {
        this.id = id;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Strint getId() {
        return id;
    }


    public String getDesc() {
        return desc;
    }

    public float getPrice() {
        return price;
    }
}
